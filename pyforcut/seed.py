"""Functions to manage GWC tile cache."""

import sys
import logging

import requests
from requests.auth import HTTPBasicAuth
import geopandas as gpd

from .config import GEOSERVER_URL, GEOSERVER_USER, GEOSERVER_PASSWORD, DATA_ROOT

logger = logging.getLogger("pyforcut.seed")


def gwc_request(
    layer_name: str,
    tile: str = "ALL",
    zoom_start: int = 0,
    zoom_stop: int = 15,
    tileset: str = "EPSG:3857x2",
    request_type: str = "RESEED",
) -> bool:
    """Reseed a tileset for an existing cached layer on a GeoServer instance."""

    if not (GEOSERVER_URL and GEOSERVER_USER and GEOSERVER_PASSWORD):
        logger.error("Cannot seed layer without geoserver credentials in env")
        return False

    if not layer_name.startswith("geonode:"):
        layer_name = "geonode:" + layer_name

    url = f"{GEOSERVER_URL}/gwc/rest/seed/{layer_name}.json"
    headers = {"Content-Type": "application/json"}

    data = {
        "seedRequest": {
            "name": layer_name,
            "gridSetId": tileset,
            "zoomStart": zoom_start,
            "zoomStop": zoom_stop,
            "type": request_type,
            "threadCount": 1,
        }
    }

    if tile != "ALL":
        gdf = gpd.read_file(DATA_ROOT / "ENVELOPE" / f"{tile}.gpkg").to_crs("EPSG:3857")
        minx, miny, maxx, maxy = gdf.total_bounds
        data["seedRequest"]["bounds"] = {"coords": {"double": [minx, miny, maxx, maxy]}}

    logger.info("Sending request for tile cache re-seed")
    try:
        auth = HTTPBasicAuth(GEOSERVER_USER, GEOSERVER_PASSWORD)
        response = requests.post(url, headers=headers, json=data, auth=auth, timeout=10)
        if response.status_code == 200:
            logger.info("GWC tileset reseed request was successful.")
            return True
        else:
            logger.error("Failed to reseed tileset. HTTP Status code: %s", response.status_code)
            logger.error(response.text)
            return False
    except requests.HTTPError as err:
        logger.error(err)
        raise SystemExit("Failed POST to REST endpoint.") from err


if __name__ == "__main__":
    sys.exit(0 if gwc_request("cbcf", "ALL") else 1)
