"""Various functions to ease the project configuration."""

import os
import logging
from datetime import datetime
from pathlib import Path

import pandas as pd
import geopandas as gpd

from sqlalchemy import (
    create_engine,
    insert,
    func,
    text,
    Column,
    Integer,
    String,
    Boolean,
    DateTime,
    ForeignKey,
    Index,
)

from sqlalchemy.orm import sessionmaker, declarative_base

from .config import DATA_ROOT, DATABASE_URL


logger = logging.getLogger("pyforcut.database")
engine = create_engine(DATABASE_URL, echo=False)
Base = declarative_base()
Session = sessionmaker(engine)


class Tile(Base):  # pylint: disable=too-few-public-methods
    """SQLAlchemy table for tiles."""

    __tablename__ = "tiles"
    fid = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(6), unique=True, nullable=False)
    enabled = Column(Boolean, nullable=False, default=True)
    created_at = Column(
        DateTime(timezone=True), server_default=func.now()  # pylint: disable=not-callable
    )


class Image(Base):  # pylint: disable=too-few-public-methods
    """SQLAlchemy table for images."""

    __tablename__ = "images"
    fid = Column(Integer, primary_key=True, autoincrement=True)
    product = Column(String(42), unique=True, nullable=False)
    tile = Column(String(6), ForeignKey("tiles.name"), nullable=False)
    year = Column(Integer, nullable=False)
    sensing_date = Column(DateTime(timezone=True), nullable=False)
    processed_at = Column(
        DateTime(timezone=True), default=func.now()  # pylint: disable=not-callable
    )
    path = Column(String, nullable=False)


class Vector(Base):  # pylint: disable=too-few-public-methods
    """SQLAlchemy table for detections."""

    __tablename__ = "vector"
    fid = Column(Integer, primary_key=True, autoincrement=True)
    tile = Column(String(6), ForeignKey("tiles.name"), nullable=False)
    year = Column(Integer, nullable=False)
    latest_date = Column(DateTime(timezone=True), nullable=True)
    feat_count = Column(Integer, nullable=False)
    processed_at = Column(
        DateTime(timezone=True), default=func.now()  # pylint: disable=not-callable
    )
    path = Column(String, nullable=True)


def init_db() -> None:
    """Initialize product database using existing tiles directories and NDVI files in data_root."""
    with Session() as session:
        Base.metadata.create_all(engine)
        Index("tile_idx", Image.tile).create(bind=engine)
        Index("product_idx", Image.product).create(bind=engine)

        ndvi_dir = Path(DATA_ROOT) / "NDVI"
        tiles_list = [path for path in ndvi_dir.iterdir() if path.is_dir()]
        if not tiles_list:
            raise ValueError(
                f"Data directory is empty, at least one tile directory has to exist in {ndvi_dir}."
            )
        tiles = [
            {"name": t.name, "created_at": datetime.fromtimestamp(os.path.getmtime(t))}
            for t in tiles_list
        ]
        session.execute(insert(Tile), tiles)

        logger.info("Scanning data directory...")
        images = []
        for tif in ndvi_dir.glob("**/*.tif"):
            name = tif.stem[:-5]
            date = datetime.strptime(name[11:30] + "000+0000", "%Y%m%d-%H%M%S-%f%z")
            images.append(
                {
                    "product": name,
                    "tile": name[-6:],
                    "year": date.year,
                    "sensing_date": date,
                    "processed_at": datetime.fromtimestamp(os.path.getmtime(tif)),
                    "path": str(tif),
                }
            )
        if images:
            session.execute(insert(Image), images)

        vector_dir = Path(DATA_ROOT) / "DETECTION"
        vector = []
        for shp in vector_dir.glob("**/*_full.shp.zip"):
            tile = shp.parent.stem
            _, year, date, _ = shp.stem.split("_")
            date = datetime.strptime(date, "%Y%m%d%H%M%S")
            gdf = gpd.read_file(str(shp).replace("_full", "_aggr"))
            vector.append(
                {
                    "tile": tile,
                    "year": year,
                    "processed_at": date,
                    "latest_date": None,
                    "feat_count": len(gdf),
                    "path": str(shp),
                }
            )
        if vector:
            session.execute(insert(Vector), vector)

        tile_last_process = text(
            """
            CREATE VIEW tile_last_process AS
            SELECT a.tile, MAX(a.processed_at) AS latest_image_proc, MAX(b.processed_at) as latest_vector_proc
            FROM images a join vector b on a.tile=b.tile GROUP BY a.tile ORDER BY latest_vector_proc;
        """
        )
        tile_images_count = text(
            """
            CREATE VIEW tile_images_count AS
            SELECT tile, COUNT(*) AS images_count FROM images GROUP BY tile;
        """
        )
        session.execute(tile_last_process)
        session.execute(tile_images_count)
        session.commit()
        logger.info("Initialized database with %s existing files.", len(images))


def get_all_tiles(prioritize: bool = False) -> list[str]:
    """Return a list of all managed tiles names.

    Args:
        prioritize: Return tiles in with the oldest processed one first

    """
    tiles = []
    if prioritize:
        tiles = pd.read_sql("select * from tile_last_process", engine)["tile"].to_list()
    if not tiles:
        with Session() as session:
            tiles = [t[0] for t in session.query(Tile.name).all()]
    return tiles


def available_images(tile: str, year: int) -> pd.DataFrame:
    """Get existing NDVI files and remove missing ones from database."""
    with Session() as session:
        history = pd.read_sql(
            session.query(Image)
            .filter(Image.tile == tile, Image.year == year)
            .order_by(Image.sensing_date)
            .statement,
            session.bind,
        )

    # Syncing db with missing files on disk, in case we manually removed a corrupted image
    logger.info("Syncing db and data directory")
    deleted = []
    session.begin()
    for row in history.itertuples():
        image = DATA_ROOT / f"NDVI/{tile}/{row.product}_NDVI.tif"
        if not image.exists():
            session.query(Image).filter(Image.fid == row.fid).delete()
            deleted.append(row.fid)
    session.commit()
    return history[~history.fid.isin(deleted)]


def clear_tile(table_name: str, tile: str):
    """Delete existing vector data in db for one tile.

    Args:
        prioritize: Return tiles in with the oldest processed one first

    """
    if engine.dialect.has_table(engine.connect(), table_name):
        with Session() as session:
            session.execute(text(f"DELETE FROM {table_name} where tile='{tile}';"))
            session.commit()


def outdated_tiles():
    """Find tiles where there are new images since latest detection."""
    tiles = pd.read_sql("SELECT * FROM tile_last_process ORDER BY latest_image_proc DESC", engine)
    return tiles[tiles.latest_image_proc > tiles.latest_vector_proc]["tile"].to_list()


def compute_cbcf_ratio():
    """Compute layer CBCF ratio by commune."""
    with Session() as session:
        session.execute(
            text(
                """
    DROP TABLE IF EXISTS commune_cbcf;
    CREATE TABLE commune_cbcf AS (
        SELECT 
            row_number() OVER (order by commune.insee_com) + 1 AS fid,
            commune.insee_com, commune.nom,
            cbcf.area / commune.aire_foret * 100 AS cbcf_ratio, commune.geometry
        FROM commune LEFT JOIN (
            SELECT cbcf.insee_com, sum(area) AS area FROM cbcf
            GROUP BY cbcf.insee_com
        ) AS cbcf ON cbcf.insee_com=commune.insee_com ORDER BY insee_com
    );
    UPDATE commune_cbcf SET cbcf_ratio = coalesce(cbcf_ratio, 0);
    ALTER TABLE commune_cbcf ADD PRIMARY KEY (fid);
    CREATE INDEX commune_cbcf_geom_idx ON commune_cbcf USING GIST(geometry);
"""
            )
        )
        session.commit()


def get_images(tile: str, start: str, end: str, max_rows: int = 254) -> pd.DataFrame:
    """Get existing images sorted, but with a limit starting from latest backward."""
    with Session() as session:
        df = (
            pd.read_sql(
                session.query(Image)
                .filter(Image.tile == tile, Image.sensing_date > start, Image.sensing_date <= end)
                .order_by(Image.sensing_date.desc())
                .limit(max_rows)
                .statement,
                session.bind,
                parse_dates=["sensing_at", "processed_at"],
            )
            .loc[::-1]  # reverse order
            .reset_index(drop=True)
        )
        df["yyyymmdd"] = df["product"].apply(lambda x: x[11:19])
        return df


if not engine.dialect.has_table(engine.connect(), "tiles"):
    init_db()
