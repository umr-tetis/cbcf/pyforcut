"""Run the 'sudden forest cover loss' detection algorithm, and aggregate data."""

import re
import sys
import datetime
import argparse
import warnings
import logging
from time import sleep
from shutil import move
from pathlib import Path
from sqlalchemy.exc import SQLAlchemyError, OperationalError


from tqdm import tqdm
import pandas as pd
import geopandas as gpd
from geopandas import GeoDataFrame

import pyotb  # pylint: disable=no-member,no-name-in-module

from .config import (
    LOG_TO_STDOUT,
    DATA_ROOT,
    TMP_ROOT,
    GEONODE_DB,
    OTB_MAX_RAM,
    CURRENT_YEAR,
    CBCF_PARAMS,
    FORWARD_TMAX,
    VEGET_START_MONTH,
    VEGET_START_DAY,
    OUTPUT_SRS,
    COMMUNE,
)
from .database import engine, Session, Image, Vector, get_all_tiles, get_images, clear_tile
from .seed import gwc_request


SHP_EXTS = ("cpg", "dbf", "prj", "shp", "shx")

logger = logging.getLogger("pyforcut.analyze")


def date_range(tile: str, year: int) -> str:
    """Compute bounds of a date range, from latest image to 4 years before."""
    max_date = datetime.date.fromisoformat(
        f"{year + 1}-{VEGET_START_MONTH:02d}-{VEGET_START_DAY:02d}"
    )
    max_date = max_date + datetime.timedelta(days=FORWARD_TMAX)
    with Session() as session:
        query = (
            session.query(Image)
            .filter(Image.tile == tile, Image.sensing_date <= max_date)
            .order_by(Image.sensing_date.desc())
            .limit(1)
        )
        if not query.count():
            raise ValueError("No images found in database for this tile and year.")
        end = query.one().sensing_date  # date of latest image within time range
        four_years = datetime.timedelta(days=int(364.25 * 4))
        return str(end - four_years), str(end)


def make_files_list(tile: str, year: int, out_dir: Path, fname: str) -> tuple:
    """Get a list of images to process and write yyyymmdd txt file."""
    dates_file = out_dir / (fname + "_dates.txt")
    start, end = date_range(tile, year)
    logger.info("Searching images between %s and %s", start, end)
    df = get_images(tile, start, end)
    dmin_dmax = (df["sensing_date"].min(), df["sensing_date"].max())
    logger.info("Will analyze %s images from %s to %s", len(df), *dmin_dmax)
    out_dir.mkdir(parents=True, exist_ok=True)
    df["yyyymmdd"].to_csv(dates_file, header=False, index=False)
    return df, dates_file


def clean_badfile(err_msg: str, tile: str) -> bool:
    """Remove a corrupted image from disk and db."""
    match_error = re.match(r".*Cannot open image (.*\.tif)\..*", err_msg)
    if not match_error:
        return False
    corrupted = Path(match_error.group(1))
    logger.warning("Process failed due to corrupted image. Removing %s.", corrupted)
    logger.warning(
        "Use 'cbcf_preprocess %s -y %s' to download it again.",
        tile,
        corrupted.name[11:15],
    )
    with (DATA_ROOT / "DETECTION" / "bad_tif_files.txt").open("a", encoding="utf-8") as buf:
        buf.write(str(corrupted) + "\n")
    corrupted.unlink(missing_ok=True)
    with Session() as session:
        session.query(Image).filter(Image.path == str(corrupted)).delete()
        session.commit()
    return True


def post_process(
    gdf: GeoDataFrame, tile: str, intersect: GeoDataFrame = None, year: int = 0
) -> tuple[GeoDataFrame]:
    """GeoPandas add fields, clean and aggregate data."""
    logger.info("Post-processing data...")
    gdf["Tile"] = tile
    gdf["Year"] = gdf["Date"].str.slice(0, 4).astype(int)
    if year:  # in case we keep only one year
        gdf = gdf[gdf["Year"] == year]
    else:  # for normal "cbcf" layer, drop polygons older than 2 years
        gdf = gdf[gdf["Year"] >= CURRENT_YEAR - 2]

    # Available fields : ['DN', 'InRange', 'Class', 'NForward', 'NBackward', 'Date', 'Surface', 'Length']
    aggr = (
        gdf.dissolve(["Tile", "Year", "Class"], aggfunc={"Date": "max"})
        .reset_index(drop=False)
        .explode(ignore_index=True)
    )
    aggr = aggr.rename({"Date": "DateMax"}, axis=1)
    if intersect is not None:
        aggr = gpd.overlay(aggr, intersect, how="intersection", keep_geom_type=True)
        # geopandas<1 : some multipolygons may sill exists, bug with keep_geom_type=True
        aggr = aggr.explode(ignore_index=True)
    aggr = aggr[aggr.geom_type == "Polygon"]
    aggr["Area"] = aggr.area
    aggr.columns = map(str.lower, aggr.columns)
    return gdf, aggr


def gdf_to_db(
    tile: str, year: int, gdf: GeoDataFrame, out_file: str, latest_date: str, seed: bool = True
) -> bool:
    """Load data to GeoNode DB and log process in 'Vector' table."""
    # Push data to geonode
    if not GEONODE_DB:
        logger.error("Cannot sync data to postgis, missing GEONODE_DB environment variable.")
        return False
    table_name = "cbcf" if year == CURRENT_YEAR else f"cbcf_{year}"
    logger.info("Removing existing polygons in table '%s'", table_name)
    clear_tile(table_name, tile)
    logger.info("Pushing new data PostGIS database")
    gdf.to_postgis(table_name, engine, if_exists="append")
    with Session() as session:
        session.add(
            Vector(
                tile=tile, year=year, latest_date=latest_date, feat_count=len(gdf), path=out_file
            )
        )
        session.commit()

    if seed:
        return gwc_request(table_name, tile)
    return True


def run_detection(tile: str, year: int, intersect: GeoDataFrame, one_year: bool = False):
    """Run the clear cuts multitemporal detection algorithm."""
    tmp_dir = TMP_ROOT / "DETECTION" / tile
    tmp_dir.mkdir(parents=True, exist_ok=True)
    now = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    fname = f"CBCF_{year}_{now}"
    tmp_file = tmp_dir / (fname + "_full.shp")
    df_images, dates_file = make_files_list(tile, year, tmp_dir, fname)
    latest_date = df_images["sensing_date"].max()

    logger.info("Running OTB ClearCutsMultitemporalDetection process...")
    try:
        pyotb.ClearCutsMultitemporalDetection(
            il=df_images["path"].to_list(),
            dates=dates_file,
            outvec=str(tmp_file),
            masksdir=str(DATA_ROOT / "MASK"),
            **CBCF_PARAMS,
            ram=OTB_MAX_RAM,
            quiet=True,
        )
    # Sometimes, a corrupted or empty NDVI file may provoke this error
    except RuntimeError as err:
        # If it fails a second time, an exception is raised, something else may have gone wrong
        if clean_badfile(str(err), tile):
            return run_detection(tile, year, intersect, one_year)
        raise err

    # Force CRS declaration since it is messed up by OTB
    gdf = gpd.read_file(tmp_file).set_crs(OUTPUT_SRS, allow_override=True)
    files = [f"{tmp_dir}/{fname}_full.{ext}" for ext in SHP_EXTS]
    logger.info("Features count before aggregation: %s", len(gdf))
    # Clean, and return early in case of empty file
    for part in files:
        Path(part).unlink(missing_ok=True)
    if len(gdf) == 0:
        with Session() as session:
            session.add(
                Vector(tile=tile, latest_date=latest_date, year=year, feat_count=0, path=None)
            )
            session.commit()
        return
    year_to_keep = year if one_year else 0
    gdf, aggr = post_process(gdf, tile, intersect, year_to_keep)

    # Save outputs
    out_dir = DATA_ROOT / "DETECTION" / tile
    out_dir.mkdir(parents=True, exist_ok=True)
    out_file = out_dir / (fname + "_full.shp.zip")
    gdf.to_file(out_file, driver="ESRI Shapefile")
    logger.info("Features count after aggregation: %s", len(aggr))
    out_aggr = out_dir / (fname + "_aggr.shp.zip")
    aggr.to_file(out_aggr, driver="ESRI Shapefile")
    move(dates_file, out_dir)

    try:
        return gdf_to_db(tile, year, aggr, str(out_file), latest_date)
    except (SQLAlchemyError, OperationalError) as err1:
        logger.warning(err1)
        logger.warning("Waiting 1mn before new attempt...")
        sleep(60)
        try:
            return gdf_to_db(tile, year, aggr, str(out_file), latest_date)
        except (SQLAlchemyError, OperationalError) as err2:
            raise SystemExit("Failed to push data to DB twice, exiting.") from err2


def load_existing_data(
    tile: str, year: int, intersect: GeoDataFrame, single_year: bool = True
) -> bool:
    """Reload aggregated data from disk to db."""

    tile_dir = Path(DATA_ROOT / "DETECTION" / tile)
    files = sorted(tile_dir.glob(f"CBCF_{year}_*_full.shp.zip"))
    if not files:
        logger.warning("Could not find data for %s, %s", tile, year)
        return

    full_file = str(files[-1])
    aggr_file = Path(full_file.replace("_full", "_aggr"))
    date_file = Path(full_file.replace("_full.shp.zip", "_dates.txt"))
    with open(date_file, encoding="utf-8") as io:
        latest_date = io.read().splitlines()[-1]
    if aggr_file.exists():
        aggr = gpd.read_file(aggr_file)
    else:  # compute and write aggr files if only full output is found
        gdf = gpd.read_file(full_file)
        year_to_keep = year if single_year else 0
        gdf, aggr = post_process(gdf, tile, intersect, year_to_keep)
        aggr.to_file(aggr_file)
    return gdf_to_db(tile, year, aggr, full_file, latest_date)


def main(
    tiles: str,
    years: list[int],
    one_year: bool = False,
    load: bool = False,
    missing: bool = False,
) -> int:
    """Run detection for a set of tiles and years."""
    Session().close()  # just check db first
    logger.info("***** Starting process *****")
    if tiles[0] == "ALL":
        tiles = get_all_tiles(prioritize=True)

    commune = gpd.read_file(COMMUNE)
    commune = commune[["NOM", "INSEE_COM", "INSEE_DEP", "INSEE_REG", "SIREN_EPCI", "geometry"]]

    iterator = iter(tiles) if LOG_TO_STDOUT else tqdm(tiles, leave=False)
    for tile in iterator:
        if not LOG_TO_STDOUT:
            iterator.set_postfix_str(tile + " ")
        for year in years:
            if load:
                logger.info("Loading existing data for tile %s and %s", tile, year)
                load_existing_data(tile, year, commune, one_year)
                continue
            if missing:
                query = f"select * from vector where tile='{tile}' and year={year}"
                existing = pd.read_sql(query, engine)
                if len(existing) > 0:
                    logger.info("%s (%s) already processed, skipping", tile, year)
                    continue
            logger.info("Processing tile %s for year %s", tile, year)
            run_detection(tile, year, commune, one_year)

    logger.info("***** Process ended successfully *****")
    return 0


def cli() -> int:
    """Init CLI and run the processing chain."""
    parser = argparse.ArgumentParser()
    parser.add_argument("tiles", nargs="+")
    parser.add_argument("-y", "--years", type=int, nargs="*", default=[CURRENT_YEAR])
    parser.add_argument("-o", "--one-year", action="store_true", help="only one year per layer")
    parser.add_argument("-l", "--load", action="store_true", help="load existing data to postgis")
    parser.add_argument("-m", "--missing", action="store_true", help="process only missing tiles")
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        return main(**vars(parser.parse_args(sys.argv[1:])))


# This enable running the module with `python -m pyforcut.detection`
if __name__ == "__main__":
    sys.exit(main(["ALL"], [CURRENT_YEAR]))
