"""Functions to run as a cron job for weekly database update."""

import sys

import geopandas as gpd
from tqdm import tqdm

# import schedule
# from sqlalchemy.exc import SQLAlchemyError

from .config import (
    logger,
    LOG_TO_STDOUT,
    COMMUNE,
    CURRENT_YEAR,
    CURRENT_MONTH,
    VEGET_START_MONTH,
    VEGET_END_MONTH,
    GEONODE_DB,
)
from .database import Session, outdated_tiles, compute_cbcf_ratio
from .analyze import run_detection
from .seed import gwc_request


def update_tiles() -> int:
    """Run detection on each tile for the current year, update data in database."""
    if not GEONODE_DB:
        raise ValueError("Cannot push data, missing GEONODE_DB environment variable.")
    if not VEGET_START_MONTH <= CURRENT_MONTH <= VEGET_END_MONTH + 1:
        return 0
    Session().close()  # just check db first

    logger.info("***** Starting weekly data processing *****")
    tiles = outdated_tiles()
    if not tiles:
        logger.info("All tiles already up-to-date.")
        return 0

    commune = gpd.read_file(COMMUNE)
    commune = commune[["NOM", "INSEE_COM", "INSEE_DEP", "INSEE_REG", "SIREN_EPCI", "geometry"]]
    iterator = iter(tiles) if LOG_TO_STDOUT else tqdm(tiles)
    for tile in iterator:
        if not LOG_TO_STDOUT:
            iterator.set_postfix_str(tile + " " * 13)
        logger.info("Computing new data for tile %s", tile)
        run_detection(tile, CURRENT_YEAR, commune)
    compute_cbcf_ratio()
    gwc_request("geonode:commune_cbcf", "ALL", zoom_stop=12)
    logger.info("***** Scheduled job ended successfully *****")
    return 0


if __name__ == "__main__":
    sys.exit(update_tiles())
