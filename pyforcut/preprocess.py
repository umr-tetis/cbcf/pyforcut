"""Main preprocessing module, for images downloading and preprocessing."""

import sys
import time
import zipfile
import datetime
import subprocess
import itertools
import argparse
import logging
from pathlib import Path
from shutil import move, rmtree
from functools import partial
from typing import Callable
from urllib3.exceptions import ReadTimeoutError, MaxRetryError

import pandas as pd
from osgeo import gdal
from tqdm import tqdm
from sqlalchemy.exc import SQLAlchemyError, OperationalError
from requests.exceptions import ReadTimeout

import theia_picker
import pyotb  # pylint: disable=no-member,no-name-in-module

from .config import (
    LOG_TO_STDOUT,
    DATA_ROOT,
    TMP_ROOT,
    ENVELOPE,
    CREDENTIALS,
    USE_SLOPE_CORRECTION,
    MAX_CLOUD_COVER,
    VEGET_END_DAY,
    VEGET_END_MONTH,
    VEGET_START_DAY,
    VEGET_START_MONTH,
    NODATA,
    CURRENT_YEAR,
    OUTPUT_SRS,
    OTB_MAX_RAM,
)
from .database import Session, Image, Tile, get_all_tiles, available_images

logger = logging.getLogger("pyforcut.preprocess")
TMP_ROOT.mkdir(exist_ok=True, parents=True)

REFLECTANCE = "FRE" if USE_SLOPE_CORRECTION else "SRE"
S2_MASKS = ["MG2_R1.tif", "SAT_R1.tif"]
S2_BANDS = [f"{REFLECTANCE}_B4.tif", f"{REFLECTANCE}_B8.tif"]
S2_METADATA = ["MTD_ALL.xml", "QKL_ALL.jpg"]
S2_FILES = S2_BANDS + S2_MASKS + S2_METADATA
# Images list = [MG2, SAT, B4, B8]
VALID_PIXEL_EXPR = "(im1b1==0) && ( ((int) im2b1 & 128) == 0) && ( ((int) im2b1 & 8) == 0)"
NDVI_EXPR = "10000*(im4b1-im3b1)/(im4b1+im3b1)"
FULL_EXPR = f"(im3b1<=0) && (im4b1<=0) ? {NODATA} : {VALID_PIXEL_EXPR} ? {NDVI_EXPR} : {NODATA}"


def find_products(
    tile: str, year: int, existing: list[str], full_search: bool = False
) -> tuple[theia_picker.Feature]:
    """Find new images to download, using config dates or list of existing images."""
    series = available_images(tile, year)
    utc = datetime.timezone.utc
    veget_start = datetime.datetime(year, VEGET_START_MONTH, VEGET_START_DAY, tzinfo=utc)
    veget_end = datetime.datetime(year, VEGET_END_MONTH, VEGET_END_DAY, tzinfo=utc)
    end = min(veget_end + datetime.timedelta(days=1), datetime.datetime.now(utc))

    if full_search or len(series) == 0:
        start = veget_start
    else:
        last = series.sensing_date.iloc[-1]
        start = pd.to_datetime(last, utc=True) + datetime.timedelta(days=1)
    if start > end:
        return ()
    try:
        catalog = theia_picker.TheiaCatalog(str(CREDENTIALS))
        features = catalog.search(start, end, level="LEVEL2A", tile_name=tile)
        return tuple(
            reversed([f for f in features if f.properties.product_identifier[:-2] not in existing])
        )
    except theia_picker.download.InvalidToken as err:
        raise SystemExit("Unable to connect to THEIA. Check your credentials.") from err
    except (TimeoutError, ConnectionError, ReadTimeoutError, MaxRetryError) as err:
        logger.error(err)
        raise TimeoutError("Unable to get list of products using current token.") from err


def process_zip(feature: theia_picker.Feature, tmp_dir: Path, archive: Path) -> tuple:
    """Download (or try open existing) zipfile with theia-picker."""
    cached_archive = Path(str(archive).replace(str(TMP_ROOT), str(DATA_ROOT / "CACHE")))
    if cached_archive.exists():
        archive = cached_archive

    if archive.exists():
        logger.info("Found cached archive, trying to open")
        try:
            with zipfile.ZipFile(archive) as zipf:
                pass
        except zipfile.BadZipFile:
            archive.unlink()
            logger.warning("Removed corrupted file %s and re-downloading", archive)
            feature.download_archive(str(tmp_dir))
    else:
        feature.download_archive(str(tmp_dir))

    with zipfile.ZipFile(archive) as zipf:
        files_list = zipf.namelist()
        tiffs = tuple(S2_BANDS + S2_MASKS)
        files = [f"{archive}/{f}" for f in files_list if str(f).endswith(tiffs)]
    return archive, sorted(files)


def vacuum(file: Path, cache_dir: Path, selective: bool, save_files: bool):
    """Clean and / or save files to NAS. If selective, file is used to get parent dir, if not file is a zip."""
    if save_files:
        if selective:
            move(file.parent, cache_dir)
        else:
            del_suffix = "SRE" if REFLECTANCE == "FRE" else "SRE"
            if file.parent != cache_dir:
                cmd = ["zip", "-d", str(file), f"*_{del_suffix}_*"]
                subprocess.run(cmd, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
                move(file, cache_dir)
    else:
        if selective:
            rmtree(file.parent)
        else:
            file.unlink()


def compute_ndvi(files: list, tmp_dir: Path, fname: str, cutline: Path) -> Path:
    """Compute NDVI, reproject image and clip to tile geom."""
    logger.info("Computing NDVI")
    utm_file = tmp_dir / (fname + "_UTM.tif")
    # Files must be ordered [MG2, SAT, B4, B8]
    app = pyotb.BandMathX(il=files, exp=FULL_EXPR, ram=OTB_MAX_RAM, quiet=True)
    success = app.write(utm_file, "int16", ext_fname=f"?&nodata={NODATA}")

    if success:
        l93_file = tmp_dir / (fname + ".tif")
        logger.info("Warping image to %s", OUTPUT_SRS)
        ds_out = gdal.Warp(
            str(l93_file),
            str(utm_file),
            dstSRS=OUTPUT_SRS,
            xRes=10,
            yRes=10,
            resampleAlg="near",
            creationOptions=["COMPRESS=DEFLATE", "PREDICTOR=2"],
            cutlineDSName=str(cutline),
            cutlineLayer=cutline.stem,
            cropToCutline=True,
            # callback=gdal.TermProgress_nocb,
        )
        if not ds_out or not l93_file.exists():
            raise RuntimeError(
                f"An error occurred during GDAL Warp. You should inspect file {utm_file}."
            )
        utm_file.unlink()  # remove cached file
        return l93_file
    raise RuntimeError("An error occurred during OTB BandMathX.")


def process_image(
    feature: theia_picker.Feature,
    tile: str,
    cleanup: bool = False,
    download_only: bool = False,
    selective: bool = False,
):
    """Download and process one Sentinel-2 product."""
    name = feature.properties.product_identifier[:-2]  # we don't keep "_D" in name
    logger.info("*** Processing %s", name)
    tmp_dir = TMP_ROOT / tile
    cache_dir = DATA_ROOT / "CACHE" / tile
    archive = tmp_dir / f"{name}_D.zip"

    # Selective download is currently broken
    if selective:
        try:
            feature.download_files(str(tmp_dir), matching=S2_FILES)
            files = sorted(tmp_dir.glob(name + "*/*.tif"))
        except theia_picker.download.RemoteZipCreationException:
            logger.warning("Selective download failed, fetching entire archive")
            return process_image(feature, tile, cleanup, download_only, not selective)
        except (TimeoutError, ReadTimeoutError, MaxRetryError, ReadTimeout) as err:
            logger.error(err)
            raise TimeoutError("Failed to download single file") from err
    else:
        try:
            archive, files = process_zip(feature, tmp_dir, archive)
        except (TimeoutError, ReadTimeoutError, MaxRetryError, ReadTimeout) as err:
            logger.error(err)
            raise TimeoutError("Unable to fetch zip file") from err

    if not download_only:
        fname = f"{name}_NDVI.tif"
        out_path = DATA_ROOT / "NDVI" / tile
        out_path.mkdir(parents=True, exist_ok=True)
        out_file = out_path / fname
        try:
            tmpfile = compute_ndvi(files, tmp_dir, out_file.stem, ENVELOPE / f"{tile}.gpkg")
            move(tmpfile, out_file)
        except FileExistsError:
            logger.error(
                "File %s already exists on NAS, db may be unsynced. Drop tables and reset index.",
                out_file,
            )
            raise
        except OSError as err:
            logger.error(err)
            logger.error("Failed to copy file because %s is either offline or full.", DATA_ROOT)
            raise

        # Log image processing in database
        date = datetime.datetime.strptime(name[11:30] + "000", "%Y%m%d-%H%M%S-%f")
        with Session() as session:
            session.add(
                Image(
                    tile=tile, product=name, year=date.year, sensing_date=date, path=str(out_file)
                )
            )
            session.commit()

    file = archive if not selective else files[0]
    vacuum(file, cache_dir, selective, save_files=download_only or not cleanup)


def retry(func: Callable, ntimes: int = 1, secs: int = 60) -> bool:
    """Sleep and wait before retrying THEIA request."""
    for i in range(ntimes):
        logger.warning("Tile process failed. Waiting %ss and retrying...", secs)
        time.sleep(secs)
        try:
            return func()
        except TimeoutError as err:
            if i + 1 == ntimes:
                logger.error(err)
                raise SystemExit(
                    f"Timed-out while requesting THEIA catalog. Exiting after {ntimes + 1} failures."
                ) from err
        except (SQLAlchemyError, OperationalError) as err:
            if i + 1 == ntimes:
                logger.error(err)
                raise SystemExit(
                    "Remote database error, check VM status and postgres server logs."
                ) from err


def refresh_tile(
    tile: str,
    year: int,
    cleanup: bool = False,
    download_only: bool = False,
    full_search: bool = False,
) -> bool:
    """Download all images for this tile. Returns bool flag that tile has been updated."""
    (TMP_ROOT / tile).mkdir(exist_ok=True)
    if not cleanup:
        cache_dir = DATA_ROOT / "CACHE" / tile
        cache_dir.mkdir(exist_ok=True)

    with Session() as session:
        df_tiles = pd.read_sql(session.query(Tile).statement, session.bind)
        df_images = pd.read_sql(
            session.query(Image).filter(Image.tile == tile).statement, session.bind
        )

    if tile not in df_tiles["name"].values:
        logger.error("Tile %s isn't in our list or does not exists.", tile)
        raise ValueError(f"Unknown tile name {tile}")

    logger.info("%s images currently stored on disk for this tile", len(df_images))

    features = find_products(tile, year, df_images["product"].to_list(), full_search)
    if not features:
        logger.info("All available images already processed for tile %s", tile)
        return False

    iterator = iter(features) if LOG_TO_STDOUT else tqdm(features, position=1, leave=False)
    # Process starts with oldest image available
    for feat in iterator:
        short_name = feat.properties.product_identifier[:19]
        if feat.properties.cloud_cover > MAX_CLOUD_COVER:
            continue

        if not LOG_TO_STDOUT:
            iterator.set_postfix_str(short_name)
        try:
            process_image(feat, tile, cleanup, download_only)
        except TimeoutError as err:
            logger.warning(err)
            retry(partial(process_image, feat, tile, cleanup, download_only), 2, 240)
        except (SQLAlchemyError, OperationalError) as err:
            logger.warning(err)
            retry(partial(process_image, feat, tile, cleanup, download_only), 1, 120)
        except OSError as err:
            raise SystemExit("Unexpected system error, check log files.") from err
    return True


def main(
    tiles: list[str],
    years: list[int],
    cleanup: bool = False,
    download_only: bool = False,
    full_search: bool = False,
) -> int:
    """Download and process a given list of tiles and years."""
    Session().close()  # just check db first

    if tiles[0] == "ALL":
        tiles = get_all_tiles()

    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    logger.info("***** Image download started at %s *****", now)
    tasks = itertools.product(tiles, years)
    iterator = tasks if LOG_TO_STDOUT else tqdm(list(tasks), position=0, leave=False)
    for tile, year in iterator:
        if not LOG_TO_STDOUT:
            iterator.set_postfix_str(tile + " " * 13)
        if not 2015 < year <= CURRENT_YEAR:
            logger.error("Wrong value for year: %s. Skipping.", year)
            continue

        refresh_tile(tile, year, cleanup, download_only, full_search)

    logger.info("***** Pre-process ended successfully *****")
    return 0


def cli() -> int:
    """Parse arguments for CLI."""
    parser = argparse.ArgumentParser()
    parser.add_argument("tiles", nargs="+")
    parser.add_argument("-y", "--years", type=int, nargs="*", default=[CURRENT_YEAR])
    parser.add_argument("-c", "--cleanup", action="store_true", help="don't save zip files to NAS")
    parser.add_argument("-d", "--download-only", action="store_true", help="just fetch zip files")
    parser.add_argument("-f", "--full-search", action="store_true", help="search full date range")
    return main(**vars(parser.parse_args(sys.argv[1:])))


# This enable running the module with `python -m pyforcut.preprocess`
if __name__ == "__main__":
    sys.exit(main(["ALL"], [CURRENT_YEAR]))
