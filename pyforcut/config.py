"""Configuration variables. Do not edit in production without without prior testing."""

import os
import datetime
import tempfile
import logging
import logging.config
from pathlib import Path


HOME = Path.home()
DATA_ROOT = Path((os.environ.get("FORCUT_DATA_ROOT") or "/data"))
TMP_ROOT = Path(tempfile.gettempdir()) / "pyforcut"

CURRENT_YEAR = datetime.datetime.now().year
CURRENT_MONTH = datetime.datetime.now().month

# S2 data
MAX_CLOUD_COVER = 100
NODATA = -20000
USE_SLOPE_CORRECTION = True
OUTPUT_SRS = "EPSG:2154"

VEGET_START_MONTH = 5
VEGET_START_DAY = 1
VEGET_END_MONTH = 9
VEGET_END_DAY = 30

FORWARD_TMAX = 62
BACKWARD_TMAX = 730

OTB_MAX_RAM = 8192
CBCF_PARAMS = {
    "range_start_month": VEGET_START_MONTH,
    "range_start_day": VEGET_START_DAY,
    "range_end_month": VEGET_END_MONTH,
    "range_end_day": VEGET_END_DAY,
    "veg_threshold": 0.4,
    "backward_nmin": 5,
    "forward_nmin": 3,
    "backward_tmax": BACKWARD_TMAX,
    "forward_tmax": FORWARD_TMAX,
    "inputscale": 0.0001,
    "sfilter": "connect",
    "sfilter_connect_size": 5,
    "nodata": -2,
}

COMMUNE = DATA_ROOT / "VECTOR" / "commune.gpkg"
ENVELOPE = DATA_ROOT / "ENVELOPE"

GEONODE_DB = os.environ.get("GEONODE_DB")
DATABASE_URL = GEONODE_DB or f"sqlite:///{HOME}/history.db"

# Ensure group has rw for all created files
os.umask(0o002)

# Env
os.environ["OTB_MAX_RAM_HINT"] = str(OTB_MAX_RAM)
# Ensure logging is set for all apps during module init - before pyotb import
os.environ["THEIAPICKER_HIDE_PROGRESS"] = "1"
os.environ["LOGLEVEL"] = "DEBUG"  # theia-picker
os.environ["OTB_LOGGER_LEVEL"] = "WARNING"
os.environ["PYOTB_LOGGER_LEVEL"] = "INFO"

LOG_TO_STDOUT = os.environ.get("LOG_TO_STDOUT")
logging_cfg = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "format": "%(asctime)s [%(name)s] - %(levelname)s : %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    },
}

if LOG_TO_STDOUT:
    logging_cfg["handlers"] = {
        "stdout": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "default",
            "stream": "ext://sys.stdout",
        }
    }
    logging_cfg["loggers"] = {"root": {"level": "INFO", "handlers": ["stdout"]}}
else:
    logging_cfg["handlers"] = {
        "stderr": {
            "class": "logging.StreamHandler",
            "level": "ERROR",
            "formatter": "default",
            "stream": "ext://sys.stderr",
        },
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "default",
            "level": "DEBUG",
            "maxBytes": 1e9,
            "backupCount": 3,
            "filename": str(DATA_ROOT / "pyforcut.log"),
        },
    }
    logging_cfg["loggers"] = {"root": {"level": "INFO", "handlers": ["stderr", "file"]}}

logging.config.dictConfig(logging_cfg)
logging.getLogger("sqlalchemy.engine").setLevel(logging.INFO)

import pyotb  # pylint: disable=wrong-import-position

logger = logging.getLogger("pyforcut.config")
pyotb.logger.handlers.pop()

CREDENTIALS = DATA_ROOT / "theia_credentials.json"
if not CREDENTIALS.exists():
    logger.error("Missing theia_credentials.json in %s or %s", DATA_ROOT, HOME)
    raise FileNotFoundError("theia_credentials.json")

GEOSERVER_URL = os.environ.get("GEOSERVER_URL")
GEOSERVER_USER = os.environ.get("GEOSERVER_USER")
GEOSERVER_PASSWORD = os.environ.get("GEOSERVER_PASSWORD")
