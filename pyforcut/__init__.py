"""Common module for two routine scripts in the FORCUT pipeline"""
__version__ = "0.11.7"

# This is to ensure os.environ setting is done before pyotb / theia-picker import in other modules
from . import config
