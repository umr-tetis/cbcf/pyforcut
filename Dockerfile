# Dockerfile to dev locally, using existing OTB base image
FROM registry.forgemia.inra.fr/umr-tetis/cbcf/pyforcut:otb-base
#RUN bash -c "env"

RUN useradd -m -u 2000 -s /bin/bash forcut
WORKDIR /home/forcut

COPY . /home/pyforcut/src
RUN cd /home/pyforcut/src && pip install ".[dev]"
USER forcut
